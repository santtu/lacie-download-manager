=============================================
 Download manager for Lacie Network Space II
=============================================

:author: Santeri Paavolainen <santtu@iki.fi>

This is documentation for *lacie-download-manager*, a Python script to
allow command-line manipulation of *Lacie Network Space II* download
list (e.g. torrents).

For up-to-date version of *lacie-download-manager* check out
https://bitbucket.org/santtu/lacie-download-manager.

**Note:** An alternate method to gain manipulate downloads on your
Lacie NAS is to get root access on it, see
http://lacie.nas-central.org/wiki/Category:Network_Space#Without_dissembling
-- then enabling Transmission remote control and using tools from
https://trac.transmissionbt.com/wiki/Scripts. Maybe - I haven't tried
that myself.

Installation
============

::

  python setup.py install

Usage
=====

::

  lacie-download-manager --help

You'll need to specify:

Where to find your Lacie box
  Use ``--host`` or set ``LACIE_HOST`` in environment, or skip to use
  the default ``networkspace.local.``.

What's your username
  Use ``--user``, set ``LACIE_USER`` or skip to use the default
  ``admin``.

What's your password
  Use ``--password``, set ``LACIE_PASSWORD`` or skip to use the
  default password ``admin``.

After this, first try:::

  lacie-download-manager --list

This should print out a list of your current downloads. If you get an
error, check above settings. The next step is to add a download:::

  lacie-download-manager *.torrent

Note that you can combine multiple actions. To do a purge, resume-all,
list and add new downloads in one go, do:::

  lacie-download-manager -lpr *.torrent

Specifying defaults
===================

You want to specify defaults? Please, use your shell for that. I'm a
ZSH user and here's my custom ``lacie-download-manager`` function:::

  function lacie-download-manager {
	  local prg=~/dev/lacie-download-manager/lacie-download-manager
	  if [[ -z "$@" ]]; then
		  $prg -lprR ~/Downloads/*.torrent(N)
	  else
		  $prg "$@"
	  fi
	  return $?
  }

That is, list, purge, resume all and remove added torrent files, and
use ZSH ``(N)`` expansion flag (ignore if no matches), if no arguments
are given. (And I use the development version for this. So you would
need to change ``prg`` to point to
``/usr/local/bin/lacie-download-manager``.)

More information
================

For more information, you can also check out
``reverse-engineering-lacie.rst`` in the ``doc`` directory.
