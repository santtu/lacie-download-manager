from setuptools import setup, find_packages
import os.path, os

if not os.path.exists('lacie-download-manager.py'):
    # needed for test.py import
    os.symlink('lacie-download-manager', 'lacie-download-manager.py')

setup(
    name = "lacie-download-manager",
    version = "0.1",
    packages = find_packages(),
    scripts = ['lacie-download-manager'],
    install_requires = ['requests', 'lxml', 'texttable', 'pytz'],
    author = "Santeri Paavolainen",
    author_email = "santtu@iki.fi",
    description = "Lacie download manager for managing Lacie Network Space II (maybe others too) downloads",
    license = "GPLv3",
    test_suite = 'nose.collector',
    setup_requires=['nose>=1.0'],
)
