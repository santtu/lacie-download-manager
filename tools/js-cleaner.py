#!/usr/bin/env python
# -*- python -*-
#
# js-cleaner.py - small program to "clean" gwt code to be more
# readable
#
# Copyright 2012 Santeri Paavolainen, Helsinki Finland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# IMPORTANT NOTE: You'll need to install "slimit" yourself for this
# script to work ("easy_install slimit" or "pip install slimit").

from slimit.parser import Parser
from slimit.visitors import nodevisitor
from slimit import ast
from sys import argv, exit, stderr

class mutatingvisitor(object):
    class mutator(object):
        def __init__(self, parent, node):
            self.parent = parent
            self.node = node

        def get(self):
            return self.node

        def set(self, new_node):
            for field, value in self.parent.__dict__.items():
                if value == self.node:
                    setattr(self.parent, field, new_node)

                if isinstance(value, list):
                    # print >>stderr, "checking field %r value %r for %r, replacing with %r" % (field, value, self.node, new_node)
                    setattr(self.parent, field, map(lambda n: new_node if n == self.node else n, value))

            self.node = new_node

        # def delete(self):
        #     self.parent._children_list = filter(lambda n: n != self.node, self.parent._children_list)
        #     self.parent = self.node = None

    def visit(self, node):
        """Returns a generator that walks all children recursively,
        returns an object that you get use .get, .set and .delete
        methods to access the underlying value."""

        if not hasattr(node, 'children'):
            return

        for child in list(node.children()):
            yield mutatingvisitor.mutator(node, child)

            for submutator in self.visit(child):
                yield submutator

def get_global_vars(tree):
    vars = {}

    # Collect top-level variable creation with initializers
    for node in tree.children(): # no descent, just top-levels
        if isinstance(node, ast.VarStatement):
            for decl in node.children():
                if decl.initializer and isinstance(decl.initializer, (ast.String, ast.Number)):
                    vars[decl.identifier.value] = decl.initializer#.value
                    decl.identifier.is_global = True

    # Remove those that are assigned within functions.. (which are
    # global vars, not constants)
    for node in nodevisitor.visit(tree):
        if isinstance(node, ast.Assign) and isinstance(node.left, ast.Identifier):
            if node.left.value in vars:
                del vars[node.left.value]

    return vars

def inline_global_vars(tree):
    vars = get_global_vars(tree)

    for mutator in mutatingvisitor().visit(tree):
        node = mutator.get()

        # if isinstance(node, ast.FunctionCall):
        #     print >>stderr, repr(dir(node))

        # print >>stderr, "%r: %r => %r, %r" % (node,
        #                                       getattr(node, 'value', None),
        #                                       getattr(node, 'value', None) in vars,
        #                                       getattr(node, 'is_global', False))

        if isinstance(node, ast.Identifier) and node.value in vars and not getattr(node, 'is_global', False):
            # print >>stderr, "replacing %r with %r" % (node, vars[node.value])
            mutator.set(vars[node.value])

    return tree

if __name__ == "__main__":
    if len(argv) != 2:
        print >>stderr, "Usage: %s FILE" % (argv[0],)
        exit(2)

    parser = Parser()
    print inline_global_vars(parser.parse(open(argv[1]).read())).to_ecma(),
