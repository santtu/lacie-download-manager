#!/usr/bin/env python
import unittest
from datetime import datetime
from pytz import timezone

manager = __import__('lacie-download-manager')

class AuthTest(unittest.TestCase):
    def setUp(self):
        super(AuthTest, self).setUp()
        self.op = manager.Operation(host=None,
                                    user='admin',
                                    password='admin')
        self.gmt = timezone('GMT')

    def test_cookies(self):
        self.assertEquals(self.op.session.cookies['LaCie_login'], 'admin')
        self.assertEquals(self.op.session.cookies['LaCie_sign'],
                          'd033e22ae348aeb5660fc2140aec35850c4da997')

    def case_auth(self, method, path, when, body,
                  expected_date_auth, expected_authentication):
        (date_auth, authentication) = self.op.date_auth(path=path,
                                                        when=when,
                                                        method=method,
                                                        body=body)

        self.assertEquals(date_auth, expected_date_auth)
        self.assertEquals(authentication, expected_authentication)

    def test_auth_get(self):
        self.case_auth('GET', '/api/v2/download?limit=-1&offset=0&',
                       datetime(2012, 12, 12, 21, 18, 19, tzinfo=self.gmt),
                       '',
                       'Wed, 12 Dec 2012 21:18:19 GMT',
                       'HMAC-SHA1-DATE YWRtaW4=:Bf33k9pkoiSDVlgKeUwBHQ9K3p4=')

    def test_auth_post(self):
        self.case_auth('POST', '/api/download/form',
                       datetime(2013, 1, 4, 20, 11, 49, tzinfo=self.gmt),
                       '',
                       'Fri, 04 Jan 2013 20:11:49 GMT',
                       'HMAC-SHA1-DATE YWRtaW4=:Us/OhSB+gakNKpB7OHeJcmHaG/0=')

    def test_auth_delete(self):
        self.case_auth('DELETE', '/api/v2/download/2',
                       datetime(2013, 1, 4, 20, 16, 42, tzinfo=self.gmt),
                       '',
                       'Fri, 04 Jan 2013 20:16:42 GMT',
                       'HMAC-SHA1-DATE YWRtaW4=:eLy4LV52XX3sRr3mxk1V1svodMI=')
    def test_auth_pause(self):
        self.case_auth('POST', '/api/v2/download/1',
                       datetime(2013, 1, 6, 7, 40, 23, tzinfo=self.gmt),
                       '<download><status>stopped</status></download>',
                       'Sun, 06 Jan 2013 07:40:23 GMT',
                       'HMAC-SHA1-DATE YWRtaW4=:TUwjbDG1LaEU6Cd/GFEfa+V+JgY=')

class ListTest(unittest.TestCase):
    def setUp(self):
        self.list = manager.List(None, user='admin', password='admin')

    def test_xml_no_downloads(self):
        self.assertEquals(self.list.downloads_from_xml('<downloads><list/></downloads>'), [])

    def test_xml_one_download(self):
        d = self.list.downloads_from_xml("""
<downloads>
<list>
<download>
<id>1</id>
<name>foo</name>
</download>
</list>
</downloads>""")

        self.assertEquals(len(d), 1)
        self.assertTrue('id' in d[0])
        self.assertTrue('name' in d[0])
        self.assertEquals(int(d[0]['id']), 1)
        self.assertEquals(d[0]['name'], 'foo')

if __name__ == '__main__':
    unittest.main()
