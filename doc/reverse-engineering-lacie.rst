===============================
 Reverse Engineering Lacie NAS
===============================

:author: Santeri Paavolainen <santtu@iki.fi>

I've written here some notes on how I proceeded with reverse
engineering the Lacie API. The actual lacie API documentation is here:
:doc:`lacie-api`.

The Story
=========

At the beginning
----------------

First, it was quite obvious that this was a browser-heavy
application.

The next step was to look how the app operated. Google Chrome web
developer tools are great for this. From this I found out that:

* There's some sort of HTTP API that the client uses to communicate
  with the actual NAS server.
* The client is written using Google Web Toolkit, and the compiled
  Javascript is both obfuscated and minified.
* There are cookies named ``LaCie_login`` and ``LaCie_sign`` that
  apparently have something to do with the user login.

Uploading a torrent file
------------------------

Then I looked more closely into what happens when a new torrent file
is added to the download list.

The actual operation that uploads torrent file and starts the download
is a single HTTP POST operation to path
``/api/v2/download/form``. There are several fields in the form
upload, but via trial and error I found out that only the following
are actually required:

``LaCie_login`` and ``LaCie_sign`` cookies in header
  Okay, you have to be logged in. I copied these from browser request.

``type=on``
  I have no idea what this means. Maybe that the "type" of operation
  is to set the download to "on" state?

``userFile``
  This is the actual torrent file uploaded. File name needs to be
  given, and ``type`` needs to be ``application/octect-stream``.

``to_path``
  The file path where to put the downloaded file. ``/Download`` is the
  default one. The path root starts at the share root (see below).

``share_id``
  Share identifier, an integer. The default OpenShare seems to be at
  id 2 on my box, at least.

``Authentication``
  This is required, and I copied it from the browser request. (But see
  below on authentication and security.)

With these, the following type of ``curl`` command will upload a
torrent (just replace the variables with correct values)::

    curl \
	-v \
	-H Expect: \
	-b "$cookie" \
	-F type=on \
	-F userFile="@$file;type=application/octet-stream" \
	-F to_path=$to_path \
	-F share_id=$share_id \
	-F Authentication="HMAC-SHA1-DATE $auth" \
	http://$lacie_ip/api/v2/download/form

Okay, it's got a flaky authentication
-------------------------------------

I used the script above for some time. I had copied cookies and the
Authentication field from a browser request, and I was expecting it to
stop working at some point due to the HMAC-SHA1-DATE authentication
header. I assumed that this header was a HMAC of username, password
and date, maybe with some obfuscation thrown in.

It was strange that it worked at all, though. There's no date header
tied to HMAC's date, and the server couldn't use current time directly
either, as that'd cause the HMAC check to fail. It might work if the
time was clamped to known boundaries (e.g. on the hour) and you had
some guarantees the the client and server clocks were at least
somewhat in sync. Plus the server would check multiple boundary values
around the current time. That'd be very cumbersome mechanism, but it
could be made to work.

Alas, the fixed headers kept and kept and kept working. As I'm writing
these notes down, the values copied from browser request have been
working perfectly for four weeks already.

Clearly this API does *not* check the authenticity of the
Authentication form field.

On the other hand, it's not possible to *omit* the field. Neither it
can be junk -- it has to match expected format, and there's some
internal coding that is checked. But the actual *HMAC* is not
checked. The field check will pass as long as the encoded
Authentication field value is syntatically valid.

Doh.

Double-doh: The ``LaCie_sign`` cookie isn't a server session id
either. There is no server-side state in the system. This header is
just some *direct hash* of the password without any server-side nonce.

Listing downloads
-----------------

Next target -- listing torrents currently being downloaded.

Using browser's debugger I found out that:

* The API is at ``/api/v2/download``, with at least parameters
  ``limit`` and ``offset``.
* We've got the usual ``LaCie_login`` and ``LaCie_sign`` cookies.
* There's two additional request headers ``AUTHENTICATION`` and
  ``DATE-AUTH``. The first one matches format of ``Authentication``
  field in torrent upload. (``DATE-AUTH`` is formatted like this:
  ``Wed, 12 Dec 2012 21:18:19 GMT``)
* Response is in XML format.

This time around the HMAC-SHA1-DATE is actually used. The values I
copied to ``curl`` script stopped working after some time. So the
server is using DATE-AUTH header to check the HMAC of AUTHENTICATION
header.

So to get this really working, I have to reverse-engineer how
DATE-AUTH and AUTHENTICATION headers are calculated.

These are done in the client, not in the server, so I can look into
the javascript files.

Reverse-engineering HMAC-SHA1-DATE
----------------------------------

This is a bit more trickier. The javascript code is obfuscated and
minified GWT code. There's a description of this process at
http://code.google.com/p/degwt/wiki/HowDeGWTWorks.

Anyway, the raw JS code isn't really readable. I didn't get the DeGWT
tool to work, so I wrote a small program that will take an obfuscated
GWT javascript file and does some de-obfuscation on it (but not all
that are described at degwt web site, just some).

The python program is at ``tools/js-cleaner.py``. Anyway, after some
de-obfuscation of the code I came upon this piece of code (note: here
and below I am using samples from my Lacie box -- yours are likely to
be different)::

  function qCc(b, c) {
    var e, f, g;
    !!nCc.length && !!oCc.length || nCc = z$b('LaCie_login') != null ? z$b('LaCie_login') : '', oCc = z$b('LaCie_sign') != null ? z$b('LaCie_sign') : '';
    f = b.f + ' ' + b.g + '\nDate: ' + c + '\n' + b.e;
    g = $wnd.b64_hmac_sha1(oCc, $wnd.Base64._utf8_encode(f));
    e = 'HMAC-SHA1-DATE ' + $wnd.Base64.encode(nCc) + ':' + g;
    return e;
  }

``qCc`` is in turn called like so::

  function rCc(b) {
    var c;
    c = zCc(lCc);
    !(jxg(b.f, 'POST') || jxg(b.f, 'PUT')) && !!b.e.length && (b.e = '');
    I5(b.c, 'AUTHENTICATION', qCc(b, c));
    I5(b.c, 'DATE-AUTH', c);
    tpd(b);
    b.b.c.Tf();
  }

The latter one sets AUTHENTICATION and DATE-AUTH fields. Variable
``c`` comes from ``zCc`` and is the date with potential skew
adjustment (parameter ``b`` is ``lCc``, which is calculated elsewhere
based on difference between server-announced time and browser-reported
time, and starts at value ``0``, e.g. no skew)::

  function zCc(b) {
    pCc();
    d = new Date();
    d.setTime(d.getTime() + b);
    return d.toUTCString();
  }

Back to ``qCc``. We know that the ``c`` parameter is current
date. Looking at it we can also see that ``nCc`` and ``oCc`` are set
to login name and password hash respectively. Let's rewrite some parts
of qCc to be a bit more understandable::

  var user_name = '', pass_hash = '';
  var get_cookie = z$b;

  function qCc(b, current_date) {
    var check_value, hmac_b64;

    if (user_name.length == 0 and get_cookie('LaCie_login') != null)
      user_name = get_cookie('LaCie_login');

    if (pass_hash.length == 0 and get_cookie('LaCie_sign') != null)
      pass_hash = get_cookie('LaCie_sign');

    check_value = b.f + ' ' + b.g + '\nDate: ' + current_date + '\n' + b.e;
    hmac_b64 = $wnd.b64_hmac_sha1(pass_hash, $wnd.Base64._utf8_encode(check_value));
    return 'HMAC-SHA1-DATE ' + $wnd.Base64.encode(user_name) + ':' + hmac_b64;
  }

What are the values of ``b.f``, ``b.g`` and ``b.e``? ``rCc`` is called
from a couple of places where it is passed either ``uCc`` or ``vCc``
object. ``vCc``
``com.lacie.communicationlayer.client.common.AutoUpdateAgentInstantiator``
and ``uCc`` is just an alternative constructor for the same
class. From constructor we'll see that ``b.g`` refers to the API
endpoint address.

Where does ``LaCie_login`` and ``LaCie_sign`` come from, then? There
are multiple references to these, but this snippet from ``htg`` is
telling::

      C$b('LaCie_login');
      E$b('LaCie_login', 'admin', null, $wnd.location.pathname);
      C$b('LaCie_sign');
      E$b('LaCie_sign', (pCc(), $wnd.hex_sha1($wnd.Base64._utf8_encode('admin'))), null, $wnd.location.pathname);

``C$b`` clears a cookie and ``E$b`` sets one. So, ``LaCie_sign`` is
SHA1 hash of UTF8 encoded string of the user's password. (Note: This
does **not** base-64 encode the password, it just uses internal utf8
encoding function from Base64 library.) You can verify this with::

  echo -m "yourpassword" | sha1sum

Back to ``qCc``. At this point the easiest way to check what the ``b``
parameter actually contains is just to insert breakpoint on that
function and inspect the passed value. I got this::

  vCc
  b: Mwc
  c: L5
  e: ""
  f: "GET"
  g: "/api/v2/download?limit=-1&offset=0&"
  __proto__: rc

and on another occasion, this::

  uCc
  b: Mwc
  c: L5
  e: ""
  f: "POST"
  g: "/api/download/form"
  __proto__: rc

Thus the ``check_value`` for these are::

  "GET" + " " + "/api/v2/download?limit=-1&offset=0&" + "\nDate: " + current_date + "\n" + ""

and::

  "POST" + " " + "/api/download/form" + "\nDate: " + current_date + "\n" + ""

I wonder though what's ``b.e`` for? It is not used for adding,
browsing or deleting downloads.

Okay, found that ``b.e`` quite a bit later. It is used for download
pause and resume. When a download is resumed the ``b`` argument is::

  vCc
  b: Mwc
  c: L5
  e: "<download><status>starting</status></download>"
  f: "POST"
  g: "/api/v2/download/1"
  __proto__: rc

So it is the ``POST`` action body (as string).

Some peculiarities
------------------

* Adding a download calls ``/api/v2/download/form``, but
  **authentication** is based on path ``/api/download/form``,
  e.g. without the API version
* Note that ``GET`` uses path with API version ...
* Removing a download uses ``DELETE`` as operation for calculating
  authentication value, but actual request uses ``GET`` with header
  ``X-HTTP-Method-Override: DELETE``. (The API **does** work with
  ``DELETE`` -- the override header is used for AJAX call from
  browsers which don't generally allow other than GET and POST methods
  to be used.)

Other random findings
=====================

* Lacie's HTTP server chokes on ``Expect`` headers. When I used
  ``curl`` during testing I had to add ``-H Expect:`` option to
  suppress curl from requesting a ``100-Continue``.
